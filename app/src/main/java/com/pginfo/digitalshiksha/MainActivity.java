package com.pginfo.digitalshiksha;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import com.pginfo.digitalshiksha.Bijli.BijliActivity;
import com.pginfo.digitalshiksha.Cloth.ClothActivity;
import com.pginfo.digitalshiksha.Furniture.FurnitureActivity;
import com.pginfo.digitalshiksha.Toolbox.ToolActivity;
import com.pginfo.digitalshiksha.kitchen.Fragment.KitchenActivity;

public class MainActivity extends AppCompatActivity {


    CardView lesson1, lesson2, lesson3, lesson4, lesson5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lesson1 = findViewById(R.id.lesson1);
        lesson1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, KitchenActivity.class);
                startActivity(intent);
            }
        });

        lesson2 = findViewById(R.id.lesson2);
        lesson2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, BijliActivity.class);
                startActivity(intent);
            }
        });

        lesson3 = findViewById(R.id.lesson3);
        lesson3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ClothActivity.class);
                startActivity(intent);
            }
        });

        lesson4 = findViewById(R.id.lesson4);
        lesson4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FurnitureActivity.class);
                startActivity(intent);
            }
        });

        lesson5 = findViewById(R.id.lesson5);
        lesson5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ToolActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        finishAffinity();
      //  super.onBackPressed();
    }
}