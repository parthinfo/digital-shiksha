package com.pginfo.digitalshiksha.Toolbox.Fragment;

import android.content.ClipData;
import android.content.ClipDescription;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pginfo.digitalshiksha.R;

public class KhelFragment extends Fragment {

    LinearLayout ll_one, ll_two, ll_three, ll_four, ll_five;
    ImageView tool1, tool2, tool3, tool4, tool5, tool6, tool7;
    private static final String IMAGE_VIEW_TOOL_ONE = "TOOL ONE";
    private static final String IMAGE_VIEW_TOOL_TWO = "TOOL TWO";
    private static final String IMAGE_VIEW_TOOL_THREE = "TOOL THREE";
    private static final String IMAGE_VIEW_TOOL_FOUR = "TOOL FOUR";
    private static final String IMAGE_VIEW_TOOL_FIVE = "TOOL FIVE";
    private static final String IMAGE_VIEW_TOOL_SIX = "TOOL SIX";
    private static final String IMAGE_VIEW_TOOL_SEVEN = "TOOL SEVEN";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_khel5, container, false);

        ll_one = view.findViewById(R.id.ll_one);
        ll_two = view.findViewById(R.id.ll_two);
        ll_three = view.findViewById(R.id.ll_three);
        ll_four = view.findViewById(R.id.ll_four);
        ll_five = view.findViewById(R.id.ll_five);

        tool1 = view.findViewById(R.id.tool1);
        tool1.setTag(IMAGE_VIEW_TOOL_ONE);
        tool2 = view.findViewById(R.id.tool2);
        tool2.setTag(IMAGE_VIEW_TOOL_TWO);
        tool3 = view.findViewById(R.id.tool3);
        tool3.setTag(IMAGE_VIEW_TOOL_THREE);
        tool4 = view.findViewById(R.id.tool4);
        tool4.setTag(IMAGE_VIEW_TOOL_FOUR);
        tool5 = view.findViewById(R.id.tool5);
        tool5.setTag(IMAGE_VIEW_TOOL_FIVE);
        tool6 = view.findViewById(R.id.tool6);
        tool6.setTag(IMAGE_VIEW_TOOL_SIX);
        tool7 = view.findViewById(R.id.tool7);
        tool7.setTag(IMAGE_VIEW_TOOL_SEVEN);


        ll_one.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {

                int action = dragEvent.getAction();
                // Handles each of the expected events
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // Determines if this View can accept the dragged data
                        if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                            // if you want to apply color when drag started to your view you can uncomment below lines
                            // to give any color tint to the View to indicate that it can accept
                            // data.

                            //  view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);//set background color to your view

                            // Invalidate the view to force a redraw in the new tint
                            //  view.invalidate();

                            // returns true to indicate that the View can accept the dragged data.
                            return true;

                        }

                        // Returns false. During the current drag and drop operation, this View will
                        // not receive events again until ACTION_DRAG_ENDED is sent.
                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        // Applies a YELLOW or any color tint to the View, when the dragged view entered into drag acceptable view
                        // Return true; the return value is ignored.

//                view.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_IN);



                        // Invalidate the view to force a redraw in the new tint
                        //              view.invalidate();

                        return true;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        // Ignore the event



                        return true;
                    case DragEvent.ACTION_DRAG_EXITED:
                        // Re-sets the color tint to blue, if you had set the BLUE color or any color in ACTION_DRAG_STARTED. Returns true; the return value is ignored.

                        //  view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

                        //If u had not provided any color in ACTION_DRAG_STARTED then clear color filter.
                        //            view.getBackground().clearColorFilter();
                        // Invalidate the view to force a redraw in the new tint
                        //          view.invalidate();

                        return true;
                    case DragEvent.ACTION_DROP:
                        // Gets the item containing the dragged data


                        ClipData.Item item = dragEvent.getClipData().getItemAt(0);

                        // Gets the text data from the item.
                        String dragData = item.getText().toString();

                        if (dragData.contains("TOOL ONE")){

             //               Toast.makeText(getContext(), "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();

                            view.invalidate();

                            View v = (View) dragEvent.getLocalState();
                            ViewGroup owner = (ViewGroup) v.getParent();
                            owner.removeView(v);//remove the dragged view
                            LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                            container.addView(v);//Add the dragged view
                            v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE

                        }
                        else {

                            //            Toast.makeText(getContext(), "No", Toast.LENGTH_SHORT).show();

                        }

                        // Displays a message containing the dragged data.

                        // Returns true. DragEvent.getResult() will return true.
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:

                        // Invalidates the view to force a redraw
                        view.invalidate();

                        // Does a getResult(), and displays what happened.
                        if (dragEvent.getResult()) {
                            //          Toast.makeText(getContext(), "The drop was handled.", Toast.LENGTH_SHORT).show();

                        }
                        else {
                            //            Toast.makeText(getContext(), "The drop didn't work.", Toast.LENGTH_SHORT).show();

                        }


                        // returns true; the value is ignored.
                        return true;

                    // An unknown action type was received.
                    default:
                        Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                        break;
                }

                return false;
            }
        });



        ll_two.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {
                int action = dragEvent.getAction();
                // Handles each of the expected events
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // Determines if this View can accept the dragged data
                        if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                            // if you want to apply color when drag started to your view you can uncomment below lines
                            // to give any color tint to the View to indicate that it can accept
                            // data.

                            //  view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);//set background color to your view

                            // Invalidate the view to force a redraw in the new tint
                            //  view.invalidate();

                            // returns true to indicate that the View can accept the dragged data.
                            return true;

                        }

                        // Returns false. During the current drag and drop operation, this View will
                        // not receive events again until ACTION_DRAG_ENDED is sent.
                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        // Applies a YELLOW or any color tint to the View, when the dragged view entered into drag acceptable view
                        // Return true; the return value is ignored.

//                view.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_IN);



                        // Invalidate the view to force a redraw in the new tint
                        //              view.invalidate();

                        return true;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        // Ignore the event



                        return true;
                    case DragEvent.ACTION_DRAG_EXITED:
                        // Re-sets the color tint to blue, if you had set the BLUE color or any color in ACTION_DRAG_STARTED. Returns true; the return value is ignored.

                        //  view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

                        //If u had not provided any color in ACTION_DRAG_STARTED then clear color filter.
                        //            view.getBackground().clearColorFilter();
                        // Invalidate the view to force a redraw in the new tint
                        //          view.invalidate();

                        return true;
                    case DragEvent.ACTION_DROP:
                        // Gets the item containing the dragged data


                        ClipData.Item item = dragEvent.getClipData().getItemAt(0);

                        // Gets the text data from the item.
                        String dragData = item.getText().toString();

                        if (dragData.contains("TOOL THREE")){

      //                      Toast.makeText(getContext(), "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();

                            view.invalidate();

                            View v = (View) dragEvent.getLocalState();
                            ViewGroup owner = (ViewGroup) v.getParent();
                            owner.removeView(v);//remove the dragged view
                            LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                            container.addView(v);//Add the dragged view
                            v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE

                        }
                        else {

                            //            Toast.makeText(getContext(), "No", Toast.LENGTH_SHORT).show();

                        }

                        // Displays a message containing the dragged data.

                        // Returns true. DragEvent.getResult() will return true.
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:

                        // Invalidates the view to force a redraw
                        view.invalidate();

                        // Does a getResult(), and displays what happened.
                        if (dragEvent.getResult()) {
                            //          Toast.makeText(getContext(), "The drop was handled.", Toast.LENGTH_SHORT).show();

                        }
                        else {
                            //            Toast.makeText(getContext(), "The drop didn't work.", Toast.LENGTH_SHORT).show();

                        }


                        // returns true; the value is ignored.
                        return true;

                    // An unknown action type was received.
                    default:
                        Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                        break;
                }

                return false;
            }
        });

        ll_three.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {

                int action = dragEvent.getAction();
                // Handles each of the expected events
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // Determines if this View can accept the dragged data
                        if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                            // if you want to apply color when drag started to your view you can uncomment below lines
                            // to give any color tint to the View to indicate that it can accept
                            // data.

                            //  view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);//set background color to your view

                            // Invalidate the view to force a redraw in the new tint
                            //  view.invalidate();

                            // returns true to indicate that the View can accept the dragged data.
                            return true;

                        }

                        // Returns false. During the current drag and drop operation, this View will
                        // not receive events again until ACTION_DRAG_ENDED is sent.
                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        // Applies a YELLOW or any color tint to the View, when the dragged view entered into drag acceptable view
                        // Return true; the return value is ignored.

//                view.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_IN);



                        // Invalidate the view to force a redraw in the new tint
                        //              view.invalidate();

                        return true;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        // Ignore the event



                        return true;
                    case DragEvent.ACTION_DRAG_EXITED:
                        // Re-sets the color tint to blue, if you had set the BLUE color or any color in ACTION_DRAG_STARTED. Returns true; the return value is ignored.

                        //  view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

                        //If u had not provided any color in ACTION_DRAG_STARTED then clear color filter.
                        //            view.getBackground().clearColorFilter();
                        // Invalidate the view to force a redraw in the new tint
                        //          view.invalidate();

                        return true;
                    case DragEvent.ACTION_DROP:
                        // Gets the item containing the dragged data


                        ClipData.Item item = dragEvent.getClipData().getItemAt(0);

                        // Gets the text data from the item.
                        String dragData = item.getText().toString();

                        if (dragData.contains("TOOL TWO")){

               //             Toast.makeText(getContext(), "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();

                            view.invalidate();

                            View v = (View) dragEvent.getLocalState();
                            ViewGroup owner = (ViewGroup) v.getParent();
                            owner.removeView(v);//remove the dragged view
                            LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                            container.addView(v);//Add the dragged view
                            v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE

                        }
                        else {

                            //            Toast.makeText(getContext(), "No", Toast.LENGTH_SHORT).show();

                        }

                        // Displays a message containing the dragged data.

                        // Returns true. DragEvent.getResult() will return true.
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:

                        // Invalidates the view to force a redraw
                        view.invalidate();

                        // Does a getResult(), and displays what happened.
                        if (dragEvent.getResult()) {
                            //          Toast.makeText(getContext(), "The drop was handled.", Toast.LENGTH_SHORT).show();

                        }
                        else {
                            //            Toast.makeText(getContext(), "The drop didn't work.", Toast.LENGTH_SHORT).show();

                        }


                        // returns true; the value is ignored.
                        return true;

                    // An unknown action type was received.
                    default:
                        Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                        break;
                }

                return false;
            }
        });

        ll_four.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {

                int action = dragEvent.getAction();
                // Handles each of the expected events
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // Determines if this View can accept the dragged data
                        if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                            // if you want to apply color when drag started to your view you can uncomment below lines
                            // to give any color tint to the View to indicate that it can accept
                            // data.

                            //  view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);//set background color to your view

                            // Invalidate the view to force a redraw in the new tint
                            //  view.invalidate();

                            // returns true to indicate that the View can accept the dragged data.
                            return true;

                        }

                        // Returns false. During the current drag and drop operation, this View will
                        // not receive events again until ACTION_DRAG_ENDED is sent.
                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        // Applies a YELLOW or any color tint to the View, when the dragged view entered into drag acceptable view
                        // Return true; the return value is ignored.

//                view.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_IN);



                        // Invalidate the view to force a redraw in the new tint
                        //              view.invalidate();

                        return true;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        // Ignore the event



                        return true;
                    case DragEvent.ACTION_DRAG_EXITED:
                        // Re-sets the color tint to blue, if you had set the BLUE color or any color in ACTION_DRAG_STARTED. Returns true; the return value is ignored.

                        //  view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

                        //If u had not provided any color in ACTION_DRAG_STARTED then clear color filter.
                        //            view.getBackground().clearColorFilter();
                        // Invalidate the view to force a redraw in the new tint
                        //          view.invalidate();

                        return true;
                    case DragEvent.ACTION_DROP:
                        // Gets the item containing the dragged data


                        ClipData.Item item = dragEvent.getClipData().getItemAt(0);

                        // Gets the text data from the item.
                        String dragData = item.getText().toString();

                        if (dragData.contains("TOOL SIX")){

        //                    Toast.makeText(getContext(), "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();

                            view.invalidate();

                            View v = (View) dragEvent.getLocalState();
                            ViewGroup owner = (ViewGroup) v.getParent();
                            owner.removeView(v);//remove the dragged view
                            LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                            container.addView(v);//Add the dragged view
                            v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE

                        }
                        else {

                            //            Toast.makeText(getContext(), "No", Toast.LENGTH_SHORT).show();

                        }

                        // Displays a message containing the dragged data.

                        // Returns true. DragEvent.getResult() will return true.
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:

                        // Invalidates the view to force a redraw
                        view.invalidate();

                        // Does a getResult(), and displays what happened.
                        if (dragEvent.getResult()) {
                            //          Toast.makeText(getContext(), "The drop was handled.", Toast.LENGTH_SHORT).show();

                        }
                        else {
                            //            Toast.makeText(getContext(), "The drop didn't work.", Toast.LENGTH_SHORT).show();

                        }


                        // returns true; the value is ignored.
                        return true;

                    // An unknown action type was received.
                    default:
                        Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                        break;
                }

                return false;
            }
        });

        ll_five.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {

                int action = dragEvent.getAction();
                // Handles each of the expected events
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // Determines if this View can accept the dragged data
                        if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                            // if you want to apply color when drag started to your view you can uncomment below lines
                            // to give any color tint to the View to indicate that it can accept
                            // data.

                            //  view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);//set background color to your view

                            // Invalidate the view to force a redraw in the new tint
                            //  view.invalidate();

                            // returns true to indicate that the View can accept the dragged data.
                            return true;

                        }

                        // Returns false. During the current drag and drop operation, this View will
                        // not receive events again until ACTION_DRAG_ENDED is sent.
                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        // Applies a YELLOW or any color tint to the View, when the dragged view entered into drag acceptable view
                        // Return true; the return value is ignored.

//                view.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_IN);



                        // Invalidate the view to force a redraw in the new tint
                        //              view.invalidate();

                        return true;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        // Ignore the event



                        return true;
                    case DragEvent.ACTION_DRAG_EXITED:
                        // Re-sets the color tint to blue, if you had set the BLUE color or any color in ACTION_DRAG_STARTED. Returns true; the return value is ignored.

                        //  view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

                        //If u had not provided any color in ACTION_DRAG_STARTED then clear color filter.
                        //            view.getBackground().clearColorFilter();
                        // Invalidate the view to force a redraw in the new tint
                        //          view.invalidate();

                        return true;
                    case DragEvent.ACTION_DROP:
                        // Gets the item containing the dragged data


                        ClipData.Item item = dragEvent.getClipData().getItemAt(0);

                        // Gets the text data from the item.
                        String dragData = item.getText().toString();

                        if (dragData.contains("TOOL FOUR")){

                      //      Toast.makeText(getContext(), "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();

                            view.invalidate();

                            View v = (View) dragEvent.getLocalState();
                            ViewGroup owner = (ViewGroup) v.getParent();
                            owner.removeView(v);//remove the dragged view
                            LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                            container.addView(v);//Add the dragged view
                            v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE

                        }
                        else {

                            //            Toast.makeText(getContext(), "No", Toast.LENGTH_SHORT).show();

                        }

                        // Displays a message containing the dragged data.

                        // Returns true. DragEvent.getResult() will return true.
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:

                        // Invalidates the view to force a redraw
                        view.invalidate();

                        // Does a getResult(), and displays what happened.
                        if (dragEvent.getResult()) {
                            //          Toast.makeText(getContext(), "The drop was handled.", Toast.LENGTH_SHORT).show();

                        }
                        else {
                            //            Toast.makeText(getContext(), "The drop didn't work.", Toast.LENGTH_SHORT).show();

                        }


                        // returns true; the value is ignored.
                        return true;

                    // An unknown action type was received.
                    default:
                        Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                        break;
                }

                return false;
            }
        });

        tool1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLong(view);

                return true;
            }
        });

        tool2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLong(view);

                return true;
            }
        });

        tool3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLong(view);

                return true;
            }
        });

        tool4.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLong(view);

                return true;
            }
        });

        tool5.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLong(view);

                return true;
            }
        });

        tool6.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLong(view);

                return true;
            }
        });

        tool7.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLong(view);

                return true;
            }
        });


        return view;
    }

    private void onLong(View view) {

        ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());

        // Create a new ClipData using the tag as a label, the plain text MIME type, and
        // the already-created item. This will create a new ClipDescription object within the
        // ClipData, and set its MIME type entry to "text/plain"
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};

        ClipData data = new ClipData(view.getTag().toString(), mimeTypes, item);

        // Instantiates the drag shadow builder.
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

        // Starts the drag
        view.startDrag(data//data to be dragged
                , shadowBuilder //drag shadow
                , view//local data about the drag and drop operation
                , 0//no needed flags
        );

    }


}