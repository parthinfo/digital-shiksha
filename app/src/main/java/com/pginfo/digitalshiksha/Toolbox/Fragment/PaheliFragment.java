package com.pginfo.digitalshiksha.Toolbox.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pginfo.digitalshiksha.R;

public class PaheliFragment extends Fragment {

    TextView txt_answer, txt_answer1,txt_answer2, txt_hindi,txt_hindi1, txt_hindi2;
    LinearLayout ll_layout_hammer, ll_layout_pana, ll_layout_plaas;
    ImageView hammer, pana, plaas;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_paheli5, container, false);

        txt_answer = view.findViewById(R.id.txt_answer);
        txt_answer1 = view.findViewById(R.id.txt_answer1);
        txt_answer2 = view.findViewById(R.id.txt_answer2);

        txt_hindi = view.findViewById(R.id.txt_hindi);
        txt_hindi1 = view.findViewById(R.id.txt_hindi1);
        txt_hindi2 = view.findViewById(R.id.txt_hindi2);

        ll_layout_hammer = view.findViewById(R.id.ll_layout_hammer);
        ll_layout_pana = view.findViewById(R.id.ll_layout_pana);
        ll_layout_plaas = view.findViewById(R.id.ll_layout_plaas);

        hammer = view.findViewById(R.id.img_view);
        pana = view.findViewById(R.id.img_view1);
        plaas = view.findViewById(R.id.img_view2);

        ll_layout_hammer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer.setVisibility(View.GONE);
                hammer.setVisibility(View.VISIBLE);
                txt_hindi.setVisibility(View.VISIBLE);
            }
        });

        ll_layout_pana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer1.setVisibility(View.GONE);
                pana.setVisibility(View.VISIBLE);
                txt_hindi1.setVisibility(View.VISIBLE);
            }
        });

        ll_layout_plaas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer2.setVisibility(View.GONE);
                plaas.setVisibility(View.VISIBLE);
                txt_hindi2.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }
}