package com.pginfo.digitalshiksha.Toolbox;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pginfo.digitalshiksha.MainActivity;
import com.pginfo.digitalshiksha.R;
import com.pginfo.digitalshiksha.Toolbox.Fragment.KahaniFragment;
import com.pginfo.digitalshiksha.Toolbox.Fragment.KarkeShikeFragment;
import com.pginfo.digitalshiksha.Toolbox.Fragment.KavitaFragment;
import com.pginfo.digitalshiksha.Toolbox.Fragment.KhelFragment;
import com.pginfo.digitalshiksha.Toolbox.Fragment.PaheliFragment;
import com.pginfo.digitalshiksha.Toolbox.Fragment.QuestionFragment;
import com.pginfo.digitalshiksha.Toolbox.Fragment.ToolFragment;

public class ToolActivity extends AppCompatActivity {

    ImageView home;
    LinearLayout kavita, kahani, khel, paheli, karke, question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tool);

        home = findViewById(R.id.home);
        kavita = findViewById(R.id.kavita);
        kahani = findViewById(R.id.kahani);
        khel = findViewById(R.id.khel);
        paheli = findViewById(R.id.paheli);
        karke = findViewById(R.id.karke);
        question = findViewById(R.id.question);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ToolActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        ToolFragment toolFragment = new ToolFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content, toolFragment);
        fragmentTransaction.commit();

        kavita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kavita.setBackgroundDrawable(getResources().getDrawable(R.drawable.select_layout_menu));
                kahani.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                khel.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                paheli.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                karke.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                question.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                KavitaFragment kavitaFragment = new KavitaFragment();
                FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction1.replace(R.id.content,kavitaFragment);
                fragmentTransaction1.commit();

            }
        });

        kahani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                kahani.setBackgroundDrawable(getResources().getDrawable(R.drawable.select_layout_menu));
                kavita.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                khel.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                paheli.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                karke.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                question.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                KahaniFragment kahaniFragment = new KahaniFragment();
                FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction2.replace(R.id.content,kahaniFragment);
                fragmentTransaction2.commit();
            }
        });

        paheli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paheli.setBackgroundDrawable(getResources().getDrawable(R.drawable.select_layout_menu));
                kahani.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                khel.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                kavita.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                karke.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                question.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                PaheliFragment paheliFragment = new PaheliFragment();
                FragmentTransaction fragmentTransaction5 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction5.replace(R.id.content,paheliFragment);
                fragmentTransaction5.commit();
            }
        });

        karke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                karke.setBackgroundDrawable(getResources().getDrawable(R.drawable.select_layout_menu));
                kahani.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                khel.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                paheli.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                kavita.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                question.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                KarkeShikeFragment karkeSikheFragment = new KarkeShikeFragment();
                FragmentTransaction fragmentTransaction3 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction3.replace(R.id.content,karkeSikheFragment);
                fragmentTransaction3.commit();
            }
        });

        question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                question.setBackgroundDrawable(getResources().getDrawable(R.drawable.select_layout_menu));
                kahani.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                khel.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                paheli.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                karke.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                kavita.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                QuestionFragment questionFragment = new QuestionFragment();
                FragmentTransaction fragmentTransaction4 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction4.replace(R.id.content,questionFragment);
                fragmentTransaction4.commit();
            }
        });

        khel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                khel.setBackgroundDrawable(getResources().getDrawable(R.drawable.select_layout_menu));
                kahani.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                kavita.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                paheli.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                karke.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                question.setBackgroundDrawable(getResources().getDrawable(R.drawable.layout_menu));
                KhelFragment khelFragment = new KhelFragment();
                FragmentTransaction fragmentTransaction6 = getSupportFragmentManager().beginTransaction();
                fragmentTransaction6.replace(R.id.content,khelFragment);
                fragmentTransaction6.commit();
            }
        });

    }
}