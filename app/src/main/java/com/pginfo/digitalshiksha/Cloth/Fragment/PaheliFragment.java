package com.pginfo.digitalshiksha.Cloth.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pginfo.digitalshiksha.R;


public class PaheliFragment extends Fragment {

    TextView txt_answer, txt_answer1,txt_answer2,txt_answer3, txt_hindi,txt_hindi1, txt_hindi2, txt_hindi3;
    LinearLayout ll_layout_woon, ll_layout_comb, ll_layout_machine, ll_layout_sui;
    ImageView woon, comb, machine, sui;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_paheli3, container, false);

        txt_answer = view.findViewById(R.id.txt_answer);
        txt_answer1 = view.findViewById(R.id.txt_answer1);
        txt_answer2 = view.findViewById(R.id.txt_answer2);
        txt_answer3 = view.findViewById(R.id.txt_answer3);

        txt_hindi = view.findViewById(R.id.txt_hindi);
        txt_hindi1 = view.findViewById(R.id.txt_hindi1);
        txt_hindi2 = view.findViewById(R.id.txt_hindi2);
        txt_hindi3 = view.findViewById(R.id.txt_hindi3);

        ll_layout_woon = view.findViewById(R.id.ll_layout_wool);
        ll_layout_comb = view.findViewById(R.id.ll_layout_comb);
        ll_layout_machine = view.findViewById(R.id.ll_layout_machine);
        ll_layout_sui = view.findViewById(R.id.ll_layout_sui);

        woon = view.findViewById(R.id.img_view);
        comb = view.findViewById(R.id.img_view1);
        machine = view.findViewById(R.id.img_view2);
        sui = view.findViewById(R.id.img_view3);

        ll_layout_woon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer.setVisibility(View.GONE);
                woon.setVisibility(View.VISIBLE);
                txt_hindi.setVisibility(View.VISIBLE);
            }
        });

        ll_layout_comb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer1.setVisibility(View.GONE);
                comb.setVisibility(View.VISIBLE);
                txt_hindi1.setVisibility(View.VISIBLE);
            }
        });

        ll_layout_machine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer2.setVisibility(View.GONE);
                machine.setVisibility(View.VISIBLE);
                txt_hindi2.setVisibility(View.VISIBLE);
            }
        });

        ll_layout_sui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer3.setVisibility(View.GONE);
                sui.setVisibility(View.VISIBLE);
                txt_hindi3.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }
}