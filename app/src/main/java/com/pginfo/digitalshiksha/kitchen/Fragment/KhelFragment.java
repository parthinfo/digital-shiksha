package com.pginfo.digitalshiksha.kitchen.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.pginfo.digitalshiksha.R;
import com.pginfo.mylibrary.LuckyWheelView;
import com.pginfo.mylibrary.Model.LuckyItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class KhelFragment extends Fragment {

    List<LuckyItem> data = new ArrayList<>();
    TextView txtData;
    ImageView img;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_khel, container, false);

        txtData = view.findViewById(R.id.txtData);
        img = view.findViewById(R.id.img);

        final LuckyWheelView luckyWheelView =(LuckyWheelView) view.findViewById(R.id.wheel);
        LuckyItem luckyItem1 = new LuckyItem();
        luckyItem1.topText = "knife";
        luckyItem1.icon = R.drawable.knife;
        luckyItem1.color = 0xffFFF3E0;
        data.add(luckyItem1);

        LuckyItem luckyItem2 = new LuckyItem();
        luckyItem2.icon = R.drawable.mortar;
        luckyItem2.topText = "Mortar";
        luckyItem2.color = 0xffFFE0B2;
        data.add(luckyItem2);

        LuckyItem luckyItem3 = new LuckyItem();
        luckyItem3.topText = "Pliers";
        luckyItem3.icon = R.drawable.chimta;
        luckyItem3.color = 0xffFFCC80;
        data.add(luckyItem3);

        LuckyItem luckyItem4 = new LuckyItem();
        luckyItem4.topText = "Kettle";
        luckyItem4.icon = R.drawable.kettle;
        luckyItem4.color = 0xffFFE0B2;
        data.add(luckyItem4);

        LuckyItem luckyItem5 = new LuckyItem();
        luckyItem5.topText = "Gas Stove";
        luckyItem5.icon = R.drawable.gas;
        luckyItem5.color = 0xffFFCC80;
        data.add(luckyItem5);

        LuckyItem luckyItem6 = new LuckyItem();
        luckyItem6.topText = "Tea Strainer";
        luckyItem6.icon = R.drawable.strainer;
        luckyItem6.color = 0xffFFE0B2;
        data.add(luckyItem6);

        LuckyItem luckyItem7 = new LuckyItem();
        luckyItem7.topText = "Rolling pin";
        luckyItem7.icon = R.drawable.rolling_pin;
        luckyItem7.color = 0xffFFCC80;
        data.add(luckyItem7);

        LuckyItem luckyItem8 = new LuckyItem();
        luckyItem8.topText = "Griddle";
        luckyItem8.icon = R.drawable.gridlle;
        luckyItem8.color = 0xffFFE0B2;
        data.add(luckyItem8);

        luckyWheelView.setData(data);
        luckyWheelView.setRound(4);

        view.findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int index = getRandomIndex();
                luckyWheelView.startLuckyWheelWithTargetIndex(index);
            }
        });

        luckyWheelView.setLuckyRoundItemSelectedListener(new LuckyWheelView.LuckyRoundItemSelectedListener() {
            @Override
            public void LuckyRoundItemSelected(int index) {
                //Toast.makeText(getContext(), data.get(index).icon, Toast.LENGTH_SHORT).show();
                String getData = data.get(index).topText;
                String getImage = String.valueOf(data.get(index).icon);
                img.setImageResource(Integer.parseInt(getImage));
                txtData.setText(getData);
            }
        });



        return view;
    }

    private int getRandomIndex() {
        Random rand = new Random();
        return rand.nextInt(data.size() - 1) + 0;
    }

    private int getRandomRound() {
        Random rand = new Random();
        return rand.nextInt(10) + 15;
    }


}