package com.pginfo.digitalshiksha.kitchen.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pginfo.digitalshiksha.R;


public class PaheliFragment extends Fragment {

    TextView txt_gas,txt_answer,txt_machis,txt_answer1;
    ImageView img_gas,img_machis;
    LinearLayout ll_answer,ll_machis;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_paheli, container, false);

        txt_gas = view.findViewById(R.id.txt_gas);
        img_gas = view.findViewById(R.id.img_gas);
        ll_answer = view.findViewById(R.id.ll_answer);
        txt_answer = view.findViewById(R.id.txt_answer);

        txt_machis = view.findViewById(R.id.txt_machis);
        txt_answer1 = view.findViewById(R.id.txt_answer1);
        img_machis = view.findViewById(R.id.img_machis);
        ll_machis = view.findViewById(R.id.ll_machis);

        ll_answer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer.setVisibility(View.GONE);
                img_gas.setVisibility(View.VISIBLE);
                txt_gas.setVisibility(View.VISIBLE);
            }
        });

        ll_machis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer1.setVisibility(View.GONE);
                img_machis.setVisibility(View.VISIBLE);
                txt_machis.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }
}