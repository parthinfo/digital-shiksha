package com.pginfo.digitalshiksha.Furniture.Fragment;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.pginfo.digitalshiksha.R;

public class KhelFragment extends Fragment {

    ImageView sandook, baksa, almari;
    ImageView shirt, neklase, tool2, torch, cap, comb, cell, tool3, belt, ring, glove, tool4;
    private static final String IMAGE_VIEW_ALMARI = "ALMARI";
    private static final String IMAGE_VIEW_BAKSA = "BAKSA";
    private static final String IMAGE_VIEW_SANDOOK = "SANDOOK";
    LinearLayout ll_sandook, ll_baksa, ll_almari;
    AlertDialog.Builder builder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_khel4, container, false);

        sandook = view.findViewById(R.id.sandook);
        baksa = view.findViewById(R.id.baksa);
        almari = view.findViewById(R.id.almari);

        ll_sandook = view.findViewById(R.id.ll_sandook);
        ll_baksa = view.findViewById(R.id.ll_baksa);
        ll_almari = view.findViewById(R.id.ll_almari);

        shirt = view.findViewById(R.id.shirt);
        shirt.setTag(IMAGE_VIEW_ALMARI);
        neklase = view.findViewById(R.id.neklase);
        neklase.setTag(IMAGE_VIEW_SANDOOK);
        tool2 = view.findViewById(R.id.tool2);
        tool2.setTag(IMAGE_VIEW_BAKSA);
        torch = view.findViewById(R.id.torch);
        torch.setTag(IMAGE_VIEW_BAKSA);
        cap = view.findViewById(R.id.cap);
        cap.setTag(IMAGE_VIEW_ALMARI);
        comb = view.findViewById(R.id.comb);
        comb.setTag(IMAGE_VIEW_ALMARI);
        cell = view.findViewById(R.id.cell);
        cell.setTag(IMAGE_VIEW_BAKSA);
        tool3 = view.findViewById(R.id.tool3);
        tool3.setTag(IMAGE_VIEW_BAKSA);
        belt = view.findViewById(R.id.belt);
        belt.setTag(IMAGE_VIEW_ALMARI);
        ring = view.findViewById(R.id.ring);
        ring.setTag(IMAGE_VIEW_SANDOOK);
        glove = view.findViewById(R.id.glove);
        glove.setTag(IMAGE_VIEW_ALMARI);
        tool4 = view.findViewById(R.id.tool4);
        tool4.setTag(IMAGE_VIEW_BAKSA);

        ll_sandook.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {

                int action = dragEvent.getAction();
                // Handles each of the expected events
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // Determines if this View can accept the dragged data
                        if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                            // if you want to apply color when drag started to your view you can uncomment below lines
                            // to give any color tint to the View to indicate that it can accept
                            // data.

                            // returns true to indicate that the View can accept the dragged data.
                            return true;

                        }

                        // Returns false. During the current drag and drop operation, this View will
                        // not receive events again until ACTION_DRAG_ENDED is sent.
                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        // Applies a YELLOW or any color tint to the View, when the dragged view entered into drag acceptable view
                        // Return true; the return value is ignored.

                        return true;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        // Ignore the event



                        return true;
                    case DragEvent.ACTION_DRAG_EXITED:
                        // Re-sets the color tint to blue, if you had set the BLUE color or any color in ACTION_DRAG_STARTED. Returns true; the return value is ignored.

                        return true;
                    case DragEvent.ACTION_DROP:
                        // Gets the item containing the dragged data


                        ClipData.Item item = dragEvent.getClipData().getItemAt(0);

                        // Gets the text data from the item.
                        String dragData = item.getText().toString();

                        if (dragData.contains("SANDOOK")){

                            Toast.makeText(getContext(), "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();

                            view.invalidate();

                            View v = (View) dragEvent.getLocalState();
                            ViewGroup owner = (ViewGroup) v.getParent();
                            owner.removeView(v);//remove the dragged view
                            LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                            container.addView(v);//Add the dragged view
                            v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE

                        }
                        else {

                        }

                        // Displays a message containing the dragged data.

                        // Returns true. DragEvent.getResult() will return true.
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:

                        // Invalidates the view to force a redraw
                        view.invalidate();

                        // Does a getResult(), and displays what happened.
                        if (dragEvent.getResult()) {
                  //          Toast.makeText(getContext(), "The drop was handled.", Toast.LENGTH_SHORT).show();

                        }
                        else {
                //            Toast.makeText(getContext(), "The drop didn't work.", Toast.LENGTH_SHORT).show();

                        }


                        // returns true; the value is ignored.
                        return true;

                    // An unknown action type was received.
                    default:
                        Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                        break;
                }

                return false;
            }
        });

        ll_baksa.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {

                int action = dragEvent.getAction();
                // Handles each of the expected events
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // Determines if this View can accept the dragged data
                        if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                            // if you want to apply color when drag started to your view you can uncomment below lines
                            // to give any color tint to the View to indicate that it can accept
                            // data.

                            // returns true to indicate that the View can accept the dragged data.
                            return true;

                        }

                        // Returns false. During the current drag and drop operation, this View will
                        // not receive events again until ACTION_DRAG_ENDED is sent.
                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        // Applies a YELLOW or any color tint to the View, when the dragged view entered into drag acceptable view
                        // Return true; the return value is ignored.


                        return true;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        // Ignore the event



                        return true;
                    case DragEvent.ACTION_DRAG_EXITED:
                        // Re-sets the color tint to blue, if you had set the BLUE color or any color in ACTION_DRAG_STARTED. Returns true; the return value is ignored.

                        return true;
                    case DragEvent.ACTION_DROP:
                        // Gets the item containing the dragged data


                        ClipData.Item item = dragEvent.getClipData().getItemAt(0);

                        // Gets the text data from the item.
                        String dragData = item.getText().toString();

                        if (dragData.contains("BAKSA")){

                            Toast.makeText(getContext(), "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();

                            view.invalidate();

                            View v = (View) dragEvent.getLocalState();
                            ViewGroup owner = (ViewGroup) v.getParent();
                            owner.removeView(v);//remove the dragged view
                            LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                            container.addView(v);//Add the dragged view
                            v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE

                        }
                        else {

               //             Toast.makeText(getContext(), "No", Toast.LENGTH_SHORT).show();

                        }

                        // Displays a message containing the dragged data.

                        // Returns true. DragEvent.getResult() will return true.
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:

                        // Invalidates the view to force a redraw
                        view.invalidate();

                        // Does a getResult(), and displays what happened.
                        if (dragEvent.getResult()) {
                     //       Toast.makeText(getContext(), "The drop was handled.", Toast.LENGTH_SHORT).show();

                        }
                        else {
                   //         Toast.makeText(getContext(), "The drop didn't work.", Toast.LENGTH_SHORT).show();

                        }


                        // returns true; the value is ignored.
                        return true;

                    // An unknown action type was received.
                    default:
                        Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                        break;
                }

                return false;
            }
        });

        ll_almari.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {

                int action = dragEvent.getAction();
                // Handles each of the expected events
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        // Determines if this View can accept the dragged data
                        if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                            // if you want to apply color when drag started to your view you can uncomment below lines
                            // to give any color tint to the View to indicate that it can accept
                            // data.

                            // returns true to indicate that the View can accept the dragged data.
                            return true;

                        }

                        // Returns false. During the current drag and drop operation, this View will
                        // not receive events again until ACTION_DRAG_ENDED is sent.
                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:
                        // Applies a YELLOW or any color tint to the View, when the dragged view entered into drag acceptable view
                        // Return true; the return value is ignored.

//                view.getBackground().setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_IN);



                        // Invalidate the view to force a redraw in the new tint
                        //              view.invalidate();

                        return true;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        // Ignore the event



                        return true;
                    case DragEvent.ACTION_DRAG_EXITED:
                        // Re-sets the color tint to blue, if you had set the BLUE color or any color in ACTION_DRAG_STARTED. Returns true; the return value is ignored.

                        return true;
                    case DragEvent.ACTION_DROP:
                        // Gets the item containing the dragged data

                        ClipData.Item item = dragEvent.getClipData().getItemAt(0);

                        // Gets the text data from the item.
                        String dragData = item.getText().toString();

                        if (dragData.contains("ALMARI")){

                            Toast.makeText(getContext(), "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();

                            view.invalidate();

                            View v = (View) dragEvent.getLocalState();
                            ViewGroup owner = (ViewGroup) v.getParent();
                            owner.removeView(v);//remove the dragged view
                            LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                            container.addView(v);//Add the dragged view
                            v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE

                        }
                        else {

                     //       Toast.makeText(getContext(), "No", Toast.LENGTH_SHORT).show();

                        }

                        // Displays a message containing the dragged data.

                        // Returns true. DragEvent.getResult() will return true.
                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:

                        // Invalidates the view to force a redraw
                        view.invalidate();

                        // Does a getResult(), and displays what happened.
                        if (dragEvent.getResult()) {
                       //     Toast.makeText(getContext(), "The drop was handled.", Toast.LENGTH_SHORT).show();

                        }
                        else {
                         //   Toast.makeText(getContext(), "The drop didn't work.", Toast.LENGTH_SHORT).show();

                        }


                        // returns true; the value is ignored.
                        return true;

                    // An unknown action type was received.
                    default:
                        Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                        break;
                }

                return false;
            }
        });

        neklase.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                onLong(view);

                return true;
            }
        });

        ring.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLong(view);

                return true;
            }
        });

        shirt.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongALMARI(view);

                return true;
            }
        });

        cap.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                onLongALMARI(view);

                return true;
            }
        });

        comb.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongALMARI(view);

                return true;
            }
        });

        belt.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongALMARI(view);

                return true;
            }
        });

        glove.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongALMARI(view);

                return true;
            }
        });

        tool2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongBaksa(view);

                return true;
            }
        });

        torch.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongBaksa(view);

                return true;
            }
        });

        cell.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongBaksa(view);

                return true;
            }
        });

        tool3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongBaksa(view);

                return true;
            }
        });

        tool4.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongBaksa(view);

                return true;
            }
        });

        return view;
    }

    private void onLongBaksa(View view) {

        ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());

        // Create a new ClipData using the tag as a label, the plain text MIME type, and
        // the already-created item. This will create a new ClipDescription object within the
        // ClipData, and set its MIME type entry to "text/plain"
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};

        ClipData data = new ClipData(view.getTag().toString(), mimeTypes, item);

        // Instantiates the drag shadow builder.
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

        // Starts the drag
        view.startDrag(data//data to be dragged
                , shadowBuilder //drag shadow
                , view//local data about the drag and drop operation
                , 0//no needed flags
        );

    }

    private void onLongALMARI(View view) {

        ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());

        // Create a new ClipData using the tag as a label, the plain text MIME type, and
        // the already-created item. This will create a new ClipDescription object within the
        // ClipData, and set its MIME type entry to "text/plain"
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};

        ClipData data = new ClipData(view.getTag().toString(), mimeTypes, item);

        // Instantiates the drag shadow builder.
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

        // Starts the drag
        view.startDrag(data//data to be dragged
                , shadowBuilder //drag shadow
                , view//local data about the drag and drop operation
                , 0//no needed flags
        );

    }

    private void onLong(View view) {

        ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());

        // Create a new ClipData using the tag as a label, the plain text MIME type, and
        // the already-created item. This will create a new ClipDescription object within the
        // ClipData, and set its MIME type entry to "text/plain"
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};

        ClipData data = new ClipData(view.getTag().toString(), mimeTypes, item);

        // Instantiates the drag shadow builder.
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);

        // Starts the drag
        view.startDrag(data//data to be dragged
                , shadowBuilder //drag shadow
                , view//local data about the drag and drop operation
                , 0//no needed flags
        );

    }


}