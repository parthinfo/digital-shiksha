package com.pginfo.digitalshiksha.Furniture.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pginfo.digitalshiksha.R;


public class PaheliFragment extends Fragment {

    LinearLayout ll_layout_almari, ll_layout_baksa, ll_layout_lock, ll_layout_door;
    TextView txt_answer, txt_answer1,txt_answer2,txt_answer3, txt_hindi,txt_hindi1, txt_hindi2, txt_hindi3;
    ImageView almari, baksa, lock, door;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_paheli4, container, false);

        ll_layout_almari = view.findViewById(R.id.ll_layout_almari);
        ll_layout_baksa = view.findViewById(R.id.ll_layout_baksa);
        ll_layout_lock = view.findViewById(R.id.ll_layout_lock);
        ll_layout_door = view.findViewById(R.id.ll_layout_darawaja);

        txt_answer = view.findViewById(R.id.txt_answer);
        txt_answer1 = view.findViewById(R.id.txt_answer1);
        txt_answer2 = view.findViewById(R.id.txt_answer2);
        txt_answer3 = view.findViewById(R.id.txt_answer3);

        txt_hindi = view.findViewById(R.id.txt_hindi);
        txt_hindi1 = view.findViewById(R.id.txt_hindi1);
        txt_hindi2 = view.findViewById(R.id.txt_hindi2);
        txt_hindi3 = view.findViewById(R.id.txt_hindi3);

        almari = view.findViewById(R.id.img_view);
        baksa = view.findViewById(R.id.img_view1);
        lock = view.findViewById(R.id.img_view2);
        door = view.findViewById(R.id.img_view3);

        ll_layout_almari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer.setVisibility(View.GONE);
                almari.setVisibility(View.VISIBLE);
                txt_hindi.setVisibility(View.VISIBLE);
            }
        });

        ll_layout_baksa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer1.setVisibility(View.GONE);
                baksa.setVisibility(View.VISIBLE);
                txt_hindi1.setVisibility(View.VISIBLE);
            }
        });

        ll_layout_lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer2.setVisibility(View.GONE);
                lock.setVisibility(View.VISIBLE);
                txt_hindi2.setVisibility(View.VISIBLE);
            }
        });

        ll_layout_door.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer3.setVisibility(View.GONE);
                door.setVisibility(View.VISIBLE);
                txt_hindi3.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }
}