package com.pginfo.digitalshiksha.Bijli.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pginfo.digitalshiksha.R;

public class PaheliFragment extends Fragment {

    LinearLayout ll_layout_ac, ll_layout_plug, ll_layout_bread;
    TextView txt_answer, txt_answer1,txt_answer2, txt_hindi,txt_hindi1, txt_hindi2;
    ImageView ac, plug, bread;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_paheli2, container, false);

        ll_layout_ac = view.findViewById(R.id.ll_layout_ac);
        ll_layout_plug = view.findViewById(R.id.ll_layout_plug);
        ll_layout_bread = view.findViewById(R.id.ll_layout_bread);

        txt_answer = view.findViewById(R.id.txt_answer);
        txt_answer1 = view.findViewById(R.id.txt_answer1);
        txt_answer2 = view.findViewById(R.id.txt_answer2);
        txt_hindi = view.findViewById(R.id.txt_hindi);
        txt_hindi1 = view.findViewById(R.id.txt_hindi1);
        txt_hindi2 = view.findViewById(R.id.txt_hindi2);

        ac = view.findViewById(R.id.img_view);
        plug = view.findViewById(R.id.img_view1);
        bread = view.findViewById(R.id.img_view2);

        ll_layout_ac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer.setVisibility(View.GONE);
                ac.setVisibility(View.VISIBLE);
                txt_hindi.setVisibility(View.VISIBLE);
            }
        });

        ll_layout_plug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer1.setVisibility(View.GONE);
                plug.setVisibility(View.VISIBLE);
                txt_hindi1.setVisibility(View.VISIBLE);
            }
        });

        ll_layout_bread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_answer2.setVisibility(View.GONE);
                bread.setVisibility(View.VISIBLE);
                txt_hindi2.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }
}