package com.pginfo.digitalshiksha.Bijli.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pginfo.digitalshiksha.R;

public class KhelFragment extends Fragment {

    ImageView comb, plug, cell, bulb, torch, machine, car, tube;
    ImageView comb_wrong, plug_right, cell_wrong, bulb_right, torch_wrong, machine_wrong, car_wrong, tube_right;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_khel2, container, false);

        comb = view.findViewById(R.id.comb);
        plug = view.findViewById(R.id.plug);
        cell = view.findViewById(R.id.cell);
        bulb = view.findViewById(R.id.bulb);
        torch = view.findViewById(R.id.torch);
        machine = view.findViewById(R.id.machine);
        car = view.findViewById(R.id.car);
        tube = view.findViewById(R.id.tube);

        comb_wrong = view.findViewById(R.id.comb_wrong);
        plug_right = view.findViewById(R.id.plug_right);
        cell_wrong = view.findViewById(R.id.cell_wrong);
        bulb_right = view.findViewById(R.id.bulb_right);
        torch_wrong = view.findViewById(R.id.torch_wrong);
        machine_wrong = view.findViewById(R.id.machine_wrong);
        car_wrong = view.findViewById(R.id.car_wrong);
        tube_right = view.findViewById(R.id.tube_right);

        comb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comb_wrong.setVisibility(View.VISIBLE);
            }
        });

        plug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                plug_right.setVisibility(View.VISIBLE);
            }
        });

        cell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cell_wrong.setVisibility(View.VISIBLE);
            }
        });

        bulb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bulb_right.setVisibility(View.VISIBLE);
            }
        });

        torch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                torch_wrong.setVisibility(View.VISIBLE);
            }
        });

        machine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                machine_wrong.setVisibility(View.VISIBLE);
            }
        });

        car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                car_wrong.setVisibility(View.VISIBLE);
            }
        });

        tube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tube_right.setVisibility(View.VISIBLE);
            }
        });

        return view;
    }
}