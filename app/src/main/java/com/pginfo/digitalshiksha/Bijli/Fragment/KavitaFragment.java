package com.pginfo.digitalshiksha.Bijli.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pginfo.digitalshiksha.R;

public class KavitaFragment extends Fragment {

    TextView right, left;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_kavita2, container, false);


        right = view.findViewById(R.id.front);
        left = view.findViewById(R.id.back);

        left.setVisibility(View.GONE);

        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fr = getFragmentManager().beginTransaction();
                fr.replace(R.id.content,new bijliGitTwoFragment());
                fr.commit();

            }
        });


        return view;
    }
}