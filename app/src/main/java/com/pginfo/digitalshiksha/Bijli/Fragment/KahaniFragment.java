package com.pginfo.digitalshiksha.Bijli.Fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pginfo.digitalshiksha.R;

public class KahaniFragment extends Fragment {

    TextView front, back;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kahani2, container, false);

        front = view.findViewById(R.id.front);
        back = view.findViewById(R.id.back);

        back.setVisibility(View.GONE);

        front.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fr = getFragmentManager().beginTransaction();
                fr.replace(R.id.content,new BijliKahaniFragment());
                fr.commit();
            }
        });

        return view;
    }
}